#!/bin/bash

cd SPIR_docker
docker build -t spir_image . 

if [ -e "/dev/isgx" ]
then
  cd ../ZT_docker
  docker build -t zt_image .
else
  echo "Skipping ZeroTrace since the device does not have SGX driver installed."
fi

cd ../XPIR_docker
docker build -t xpir_image .
