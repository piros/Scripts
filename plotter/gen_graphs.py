#!/usr/bin/python3
import sys
import os
import subprocess
import numpy as np
from numpy import genfromtxt
import numpy.polynomial.polynomial as poly
from operator import add
import operator
#import matplotlib as mpl
#if os.environ.get('DISPLAY','') == '':
#    mpl.use('Agg')
import matplotlib.pyplot as plt

def quadratic_function(x, a, b, c):
    return a*x*x + b*x + c

def linear_function(x, a, b):
    return a*x + b

PLOT_PATHORAM = False
PLOT_LSIPRM = False
PLOT_CIRCUITORAM = False
PLOT_ZEROTRACE = False

if(len(sys.argv)>1):
  if(int(sys.argv[1])==1):
    PLOT_ZEROTRACE = True

if(PLOT_ZEROTRACE):
  PLOT_CIRCUITORAM = True

#List of plot files
LF_PATHORAM='log_ZTPATHORAM'
LF_CIRCUITORAM='log_ZTCIRCUITORAM'
LF_LSIPRM='log_ZTLSORAM_IPRM'
LF_LSOPRM='log_ZTLSORAM'
FOLDER_XPIR='./XPIR/'
LF_SEALPIR='log_SEALPIR'
LF_SEALPIR_D1='log_SEALPIR_D1'
DESC_SIZE=16384

n= 10
n_stop = 12000
N = []
while(n < n_stop):
  N.append(n)
  n = int(n * 8 / 5)
print(N)

plt.rc('font', size=18)          # controls default text sizes
plt.rc('axes', titlesize=18)     # fontsize of the axes title
plt.rc('axes', labelsize=18)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=16)    # fontsize of the tick labels
plt.rc('ytick', labelsize=16)    # fontsize of the tick labels
plt.rc('legend', fontsize=14)    # legend fontsize
plt.rc('figure', titlesize=18)  # fontsize of the figure title

########################

LSIPRM_gtime=[]
LSIPRM_ptime=[]
LSIPRM_etime=[]
LSIPRM_gtime_err=[]
LSIPRM_ptime_err=[]
LSIPRM_etime_err=[]
LSIPRM_query_size=[]
LSIPRM_response_size=[]
LSIPRM_time = []
LSIPRM_time_err = []

LSOPRM_gtime=[]
LSOPRM_ptime=[]
LSOPRM_etime=[]
LSOPRM_gtime_err=[]
LSOPRM_ptime_err=[]
LSOPRM_etime_err=[]
LSOPRM_query_size=[]
LSOPRM_response_size=[]
LSOPRM_time = []
LSOPRM_time_err = []

CircuitORAM_gtime=[]
CircuitORAM_ptime=[]
CircuitORAM_etime=[]
CircuitORAM_gtime_err=[]
CircuitORAM_ptime_err=[]
CircuitORAM_etime_err=[]
CircuitORAM_query_size=[]
CircuitORAM_response_size=[]
CircuitORAM_time = []
CircuitORAM_time_err = []

PathORAM_gtime=[]
PathORAM_ptime=[]
PathORAM_etime=[]
PathORAM_gtime_err=[]
PathORAM_ptime_err=[]
PathORAM_etime_err=[]
PathORAM_query_size=[]
PathORAM_response_size=[]
PathORAM_time = []
PathORAM_time_err = []

XPIR_gtime=[]
XPIR_ptime=[]
XPIR_etime=[]
XPIR_gtime_err=[]
XPIR_ptime_err=[]
XPIR_etime_err=[]
XPIR_query_size=[]
XPIR_response_size=[]
XPIR_time = []
XPIR_time_err =[]

SealPIR_gtime=[]
SealPIR_ptime=[]
SealPIR_etime=[]
SealPIR_gtime_err=[]
SealPIR_ptime_err=[]
SealPIR_etime_err=[]
SealPIR_query_size=[]
SealPIR_response_size=[]
SealPIR_time = []
SealPIR_time_err = []

POINT_STYLES = [".","v","s","+","x","d",">","^","*","o","D"]

########################

# NOTE N just needs to be picked once, 
# All plot files should have the same N range

if(PLOT_PATHORAM):
  with open(LF_PATHORAM, 'r') as lfile:
    for line in lfile:
      values=line.split(',')
      PathORAM_gtime.append(float(values[1]))
      PathORAM_gtime_err.append(float(values[2]))
      PathORAM_ptime.append(float(values[3]))
      PathORAM_ptime_err.append(float(values[4]))
      PathORAM_etime.append(float(values[5]))
      PathORAM_etime_err.append(float(values[6]))
      PathORAM_query_size.append(int(values[7]))
      PathORAM_response_size.append(int(values[8]))

if(PLOT_CIRCUITORAM):
  with open(LF_CIRCUITORAM, 'r') as lfile:
    for line in lfile:
      values=line.split(',')
      CircuitORAM_gtime.append(float(values[1]))
      CircuitORAM_gtime_err.append(float(values[2]))
      CircuitORAM_ptime.append(float(values[3]))
      CircuitORAM_ptime_err.append(float(values[4]))
      CircuitORAM_etime.append(float(values[5]))
      CircuitORAM_etime_err.append(float(values[6]))
      #CircuitORAM_query_size = int(values[7])
      #CircuitORAM_response_size = int(values[8])

if(PLOT_LSIPRM):
  with open(LF_LSIPRM, 'r') as lfile:
    for line in lfile:
      values=line.split(',')
      LSIPRM_gtime.append(float(values[1]))
      LSIPRM_gtime_err.append(float(values[2]))
      LSIPRM_ptime.append(float(values[3]))
      LSIPRM_ptime_err.append(float(values[4]))
      LSIPRM_etime.append(float(values[5]))
      LSIPRM_etime_err.append(float(values[6]))
      #LSIPRM_query_size = int(values[7])
      #LSIPRM_response_size = int(values[8])

if(PLOT_ZEROTRACE):
  with open(LF_LSOPRM, 'r') as lfile:
    for line in lfile:
      values=line.split(',')
      LSOPRM_gtime.append(float(values[1]))
      LSOPRM_gtime_err.append(float(values[2]))
      LSOPRM_ptime.append(float(values[3]))
      LSOPRM_ptime_err.append(float(values[4]))
      LSOPRM_etime.append(float(values[5]))
      LSOPRM_etime_err.append(float(values[6]))
      LSOPRM_query_size.append(int(values[7]))
      LSOPRM_response_size.append(int(values[8]))

#SealPIR logs don't contain N
with open(LF_SEALPIR, 'r') as lfile:
  for line in lfile:
    values = line.split(',') 
    SealPIR_gtime.append(float(values[0]))
    SealPIR_gtime_err.append(float(values[1]))
    SealPIR_ptime.append(float(values[2]))
    SealPIR_ptime_err.append(float(values[3]))
    SealPIR_etime.append(float(values[4]))
    SealPIR_etime_err.append(float(values[5]))
    SealPIR_query_size.append(int(values[6]))
    SealPIR_response_size.append(int(values[7]))
    SealPIR_PPT=float(values[8])

########################


XPIR_QE_TIME=[]
XPIR_RE_TIME=[]
XPIR_PQ_TIME=[]
XPIR_REQUEST_SIZE=[]
XPIR_RESPONSE_SIZE=[]
XPIR_QE_STD=[]
XPIR_RE_STD=[]
XPIR_PQ_STD=[]
XPIR_CLIENT_TIME=[]
XPIR_CLIENT_STD=[]
XPIR_d=[]
XPIR_alpha=[]


for n in N:
  FILE_NAME=FOLDER_XPIR+"XPIR_"+str(n)+"_"+str(DESC_SIZE)+"_0"
  qe_time=[]
  re_time=[]
  pq_time=[]
  client_time=[]
  request_size=0
  response_size=0
  d=0
  alpha=0
  with open(FILE_NAME,'r') as file_handle:
    ctr=0

    for line in file_handle:      
      if(ctr!=0):
        words=(line.strip()).split(',')
        qe_time.append(float(words[0]))
        d=int(words[4])
        alpha=int(words[5]) 	
        pq_time.append(float(words[6]))
        re_time.append(float(words[1]))
        client_time.append(float(words[0])+float(words[1]))
        request_size=(float(words[2]))
        response_size=(float(words[3]))
      ctr=ctr+1

    XPIR_REQUEST_SIZE.append(request_size)
    XPIR_RESPONSE_SIZE.append(response_size)
    XPIR_QE_TIME.append(np.mean(qe_time))
    XPIR_PQ_TIME.append(np.mean(pq_time))
    XPIR_RE_TIME.append(np.mean(re_time))
    XPIR_CLIENT_TIME.append(np.mean(client_time))
    XPIR_QE_STD.append(np.std(qe_time))
    XPIR_PQ_STD.append(np.std(pq_time))
    XPIR_RE_STD.append(np.std(re_time))
    XPIR_CLIENT_STD.append(np.std(client_time))
    XPIR_d.append(d)
    XPIR_alpha.append(alpha)

########################


LSIPRM_ctime = np.add(LSIPRM_gtime, LSIPRM_etime)
LSIPRM_ctime_err = np.add(LSIPRM_gtime_err, LSIPRM_etime_err)
LSOPRM_ctime = np.add(LSOPRM_gtime, LSOPRM_etime)
LSOPRM_ctime_err = np.add(LSOPRM_gtime_err, LSOPRM_etime_err)
PathORAM_ctime = np.add(PathORAM_gtime, PathORAM_etime)
PathORAM_ctime_err = np.add(PathORAM_gtime_err, PathORAM_etime_err)
CircuitORAM_ctime = np.add(CircuitORAM_gtime, CircuitORAM_etime)
CircuitORAM_ctime_err = np.add(CircuitORAM_gtime_err, CircuitORAM_etime_err)
XPIR_ctime = np.add(XPIR_QE_TIME, XPIR_RE_TIME)
XPIR_ctime_err = np.add(XPIR_QE_STD, XPIR_RE_STD)
SealPIR_ctime = np.add(SealPIR_gtime, SealPIR_etime)
SealPIR_ctime_err = np.add(SealPIR_gtime_err, SealPIR_etime_err)

LSIPRM_time = np.add(LSIPRM_ctime, LSIPRM_ptime)
LSIPRM_time_err = np.add(LSIPRM_ctime_err, LSIPRM_ptime_err)
LSOPRM_time = np.add(LSOPRM_ctime, LSOPRM_ptime)
LSOPRM_time_err = np.add(LSOPRM_ctime_err, LSOPRM_ptime_err)
PathORAM_time = np.add(PathORAM_ctime, PathORAM_ptime)
PathORAM_time_err = np.add(PathORAM_ctime_err, PathORAM_ptime_err)
CircuitORAM_time = np.add(CircuitORAM_ctime, CircuitORAM_ptime)
CircuitORAM_time_err = np.add(CircuitORAM_ctime_err, CircuitORAM_ptime_err)
XPIR_time = np.add(XPIR_ctime, XPIR_PQ_TIME)
XPIR_time_err = np.add(XPIR_ctime_err, XPIR_PQ_STD)
SealPIR_time = np.add(SealPIR_ctime, SealPIR_ptime)
SealPIR_time_err = np.add(SealPIR_ctime_err, SealPIR_ptime_err)

plt.xlabel('Number of Hidden Service Descriptors')
plt.ylabel('Server computation time per query (in ms)')
plt.grid(True)
plt.yscale('log')
plt.xscale('log')

c_ctr=0

if(PLOT_PATHORAM):
  points1 = plt.plot(N, PathORAM_ptime, POINT_STYLES[c_ctr], label='PathORAM',  color = str("C")+str(c_ctr))
if(PLOT_CIRCUITORAM):
  points2 = plt.plot(N, CircuitORAM_ptime, POINT_STYLES[c_ctr+1], label='CircuitORAM', color = str("C")+str(c_ctr+1))
if(PLOT_LSIPRM):
  points3 = plt.plot(N, LSIPRM_ptime, POINT_STYLES[c_ctr+2], label='Linear Scan (inside PRM)', color = str("C")+str(c_ctr+2))
if(PLOT_ZEROTRACE):
  points4 = plt.plot(N, LSOPRM_ptime, POINT_STYLES[c_ctr+3], label='Linear Scan (outside PRM)', color = str("C")+str(c_ctr+3))
points5 = plt.plot(N, XPIR_PQ_TIME, POINT_STYLES[c_ctr+4], label='XPIR', color = str("C")+str(c_ctr+4))
points6 = plt.plot(N, SealPIR_ptime, POINT_STYLES[c_ctr+5], label='SealPIR', color = str("C")+str(c_ctr+5))
points7 = plt.plot(N, SealPIR_ptime, POINT_STYLES[c_ctr+6], label='Trivial PIR', color = str("C")+str(c_ctr+6))


if(PLOT_PATHORAM):
  errbar1 = plt.errorbar(N, PathORAM_ptime, PathORAM_ptime_err, None, 'r', ls='none', mew=3)
if(PLOT_CIRCUITORAM):
  errbar2 = plt.errorbar(N, CircuitORAM_ptime, CircuitORAM_ptime_err, None, 'r', ls='none', mew=3)
if(PLOT_LSIPRM):
  errbar3 = plt.errorbar(N, LSIPRM_ptime, LSIPRM_ptime_err, None, 'r', ls='none', mew=3)
if(PLOT_ZEROTRACE):
  errbar4 = plt.errorbar(N, LSOPRM_ptime, LSOPRM_ptime_err, None, 'r', ls='none', mew=3)
errbar5 = plt.errorbar(N, XPIR_PQ_TIME, XPIR_PQ_STD, None, 'r', ls='none', mew=3)
errbar6 = plt.errorbar(N, SealPIR_ptime, SealPIR_ptime_err , None, 'r', ls='none', mew=3)


if(PLOT_PATHORAM):
  plt.setp(points1, 'mew', '3.0')
if(PLOT_CIRCUITORAM):
  plt.setp(points2, 'mew', '3.0')
if(PLOT_LSIPRM):
  plt.setp(points3, 'mew', '3.0')
if(PLOT_ZEROTRACE):
  plt.setp(points4, 'mew', '3.0')
plt.setp(points5, 'mew', '3.0')
plt.setp(points6, 'mew', '3.0')
plt.setp(points7, 'mew', '0.0')
#plt.setp(line1, 'linewidth', '2.0')
#plt.setp(line2, 'linewidth', '2.0')

ax = plt.subplot()
handles, labels = ax.get_legend_handles_labels()
labels_sort = [3,4,5,6,2,1,7]
hl = sorted(zip(handles, labels, labels_sort),
            key=operator.itemgetter(2))
handles2, labels2, labels_sort= zip(*hl)
#ax.legend(handles2, labels2, loc = 7)

plt.title('Server computation time (in ms) vs \nnumber of hidden service descriptors')
ax.legend(handles2, labels2, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=7, shadow=True, handletextpad=0.1, columnspacing=0.5)
plt.savefig('server_time_microbenchmark.png', bbox_inches='tight')
plt.close()

############################################################

plt.xlabel('Number of Hidden Service Descriptors')
plt.ylabel('Client query generation time per query (in ms)')
plt.grid(True)
plt.yscale('log')
plt.xscale('log')

c_ctr=0

if(PLOT_PATHORAM):
  points1 = plt.plot(N, PathORAM_ctime, POINT_STYLES[c_ctr], label='PathORAM',  color = str("C")+str(c_ctr))
if(PLOT_CIRCUITORAM):
  points2 = plt.plot(N, CircuitORAM_ctime, POINT_STYLES[c_ctr+1], label='CircuitORAM', color = str("C")+str(c_ctr+1))
if(PLOT_LSIPRM):
  points3 = plt.plot(N, LSIPRM_ctime, POINT_STYLES[c_ctr+2], label='Linear Scan (inside PRM)', color = str("C")+str(c_ctr+2))
if(PLOT_ZEROTRACE):
  points4 = plt.plot(N, LSOPRM_gtime, POINT_STYLES[c_ctr+3], label='ZeroTrace', color = str("C")+str(c_ctr+3))
points5 = plt.plot(N, XPIR_QE_TIME, POINT_STYLES[c_ctr+4], label='XPIR', color = str("C")+str(c_ctr+4))
points6 = plt.plot(N, SealPIR_gtime, POINT_STYLES[c_ctr+5], label='SealPIR', color = str("C")+str(c_ctr+5))

if(PLOT_PATHORAM):
  errbar1 = plt.errorbar(N, PathORAM_ctime, PathORAM_ctime_err, None, 'r', ls='none', mew=3)
if(PLOT_CIRCUITORAM):
 errbar2 = plt.errorbar(N, CircuitORAM_ctime, CircuitORAM_ctime_err, None, 'r', ls='none', mew=3)
if(PLOT_LSIPRM):
  errbar3 = plt.errorbar(N, LSIPRM_ctime, LSIPRM_ctime_err, None, 'r', ls='none', mew=3)
if(PLOT_ZEROTRACE):
  errbar4 = plt.errorbar(N, LSOPRM_gtime, LSOPRM_gtime_err, None, 'r', ls='none', mew=3)
errbar5 = plt.errorbar(N, XPIR_QE_TIME, XPIR_QE_STD, None, 'r', ls='none', mew=3)
errbar6 = plt.errorbar(N, SealPIR_gtime, SealPIR_gtime_err , None, 'r', ls='none', mew=3)

if(PLOT_PATHORAM):
  plt.setp(points1, 'mew', '3.0')
if(PLOT_CIRCUITORAM):
  plt.setp(points2, 'mew', '3.0')
if(PLOT_LSIPRM):
  plt.setp(points3, 'mew', '3.0')
if(PLOT_ZEROTRACE):
  plt.setp(points4, 'mew', '3.0')
plt.setp(points5, 'mew', '3.0')
plt.setp(points6, 'mew', '3.0')
#plt.setp(line1, 'linewidth', '2.0')
#plt.setp(line2, 'linewidth', '2.0')

ax = plt.subplot()
handles, labels = ax.get_legend_handles_labels()
labels_sort = [3,2,1]
hl = sorted(zip(handles, labels, labels_sort),
            key=operator.itemgetter(2))
handles2, labels2, labels_sort= zip(*hl)
#ax.legend(handles2, labels2, loc = 7)

plt.title('Client query generation time (in ms) \nvs number of hidden service descriptors')
ax.legend(handles2, labels2, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=2, shadow=True)
plt.savefig('client_query_time_microbenchmark.png', bbox_inches='tight')
plt.close()

print("Done with client query time graph..")
############################################################

plt.xlabel('Number of Hidden Service Descriptors')
plt.ylabel('Client reply extraction time per query (in ms)')
plt.grid(True)
plt.yscale('log')
plt.xscale('log')

c_ctr=0

if(PLOT_PATHORAM):
  points1 = plt.plot(N, PathORAM_ctime, POINT_STYLES[c_ctr], label='PathORAM',  color = str("C")+str(c_ctr))
if(PLOT_CIRCUITORAM):
  points2 = plt.plot(N, CircuitORAM_ctime, POINT_STYLES[c_ctr+1], label='CircuitORAM', color = str("C")+str(c_ctr+1))
if(PLOT_LSIPRM):
  points3 = plt.plot(N, LSIPRM_ctime, POINT_STYLES[c_ctr+2], label='Linear Scan (inside PRM)', color = str("C")+str(c_ctr+2))
if(PLOT_ZEROTRACE):
  points4 = plt.plot(N, LSOPRM_etime, POINT_STYLES[c_ctr+3], label='ZeroTrace', color = str("C")+str(c_ctr+3))
points5 = plt.plot(N, XPIR_RE_TIME, POINT_STYLES[c_ctr+4], label='XPIR', color = str("C")+str(c_ctr+4))
points6 = plt.plot(N, SealPIR_etime, POINT_STYLES[c_ctr+5], label='SealPIR', color = str("C")+str(c_ctr+5))

if(PLOT_PATHORAM):
  errbar1 = plt.errorbar(N, PathORAM_ctime, PathORAM_ctime_err, None, 'r', ls='none', mew=3)
if(PLOT_CIRCUITORAM):
  errbar2 = plt.errorbar(N, CircuitORAM_ctime, CircuitORAM_ctime_err, None, 'r', ls='none', mew=3)
if(PLOT_LSIPRM):
  errbar3 = plt.errorbar(N, LSIPRM_ctime, LSIPRM_ctime_err, None, 'r', ls='none', mew=3)
if(PLOT_ZEROTRACE):
  errbar4 = plt.errorbar(N, LSOPRM_etime, LSOPRM_etime_err, None, 'r', ls='none', mew=3)
errbar5 = plt.errorbar(N, XPIR_RE_TIME, XPIR_RE_STD, None, 'r', ls='none', mew=3)
errbar6 = plt.errorbar(N, SealPIR_etime, SealPIR_etime_err , None, 'r', ls='none', mew=3)

if(PLOT_PATHORAM):
  plt.setp(points1, 'mew', '3.0')
if(PLOT_CIRCUITORAM):
  plt.setp(points2, 'mew', '3.0')
if(PLOT_LSIPRM):
  plt.setp(points3, 'mew', '3.0')
if(PLOT_ZEROTRACE):
  plt.setp(points4, 'mew', '3.0')
plt.setp(points5, 'mew', '3.0')
plt.setp(points6, 'mew', '3.0')
#plt.setp(line1, 'linewidth', '2.0')
#plt.setp(line2, 'linewidth', '2.0')

ax = plt.subplot()
handles, labels = ax.get_legend_handles_labels()
labels_sort = [3,2,1]
hl = sorted(zip(handles, labels, labels_sort),
            key=operator.itemgetter(2))
handles2, labels2, labels_sort= zip(*hl)
#ax.legend(handles2, labels2, loc = 7)

plt.title('Client reply extraction time (in ms) \nvs number of hidden service descriptors')
ax.legend(handles2, labels2, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=2, shadow=True)
plt.savefig('client_reply_time_microbenchmark.png', bbox_inches='tight')
plt.close()

print("Done with client reply extraction time graph..")

############################################################

plt.xlabel('Number of Hidden Service Descriptors')
plt.ylabel('Client computation time per query (in ms)')
plt.grid(True)
plt.yscale('log')
plt.xscale('log')

c_ctr=0

if(PLOT_PATHORAM):
  points1 = plt.plot(N, PathORAM_ctime, POINT_STYLES[c_ctr], label='PathORAM',  color = str("C")+str(c_ctr))
if(PLOT_CIRCUITORAM):
  points2 = plt.plot(N, CircuitORAM_ctime, POINT_STYLES[c_ctr+1], label='CircuitORAM', color = str("C")+str(c_ctr+1))
if(PLOT_LSIPRM):
  points3 = plt.plot(N, LSIPRM_ctime, POINT_STYLES[c_ctr+2], label='Linear Scan (inside PRM)', color = str("C")+str(c_ctr+2))
if(PLOT_ZEROTRACE):
  points4 = plt.plot(N, LSOPRM_ctime, POINT_STYLES[c_ctr+3], label='ZeroTrace', color = str("C")+str(c_ctr+3))
points5 = plt.plot(N, XPIR_ctime, POINT_STYLES[c_ctr+4], label='XPIR', color = str("C")+str(c_ctr+4))
points6 = plt.plot(N, SealPIR_ctime, POINT_STYLES[c_ctr+5], label='SealPIR', color = str("C")+str(c_ctr+5))

if(PLOT_PATHORAM):
  errbar1 = plt.errorbar(N, PathORAM_ctime, PathORAM_ctime_err, None, 'r', ls='none', mew=3)
if(PLOT_CIRCUITORAM):
  errbar2 = plt.errorbar(N, CircuitORAM_ctime, CircuitORAM_ctime_err, None, 'r', ls='none', mew=3)
if(PLOT_LSIPRM):
  errbar3 = plt.errorbar(N, LSIPRM_ctime, LSIPRM_ctime_err, None, 'r', ls='none', mew=3)
if(PLOT_ZEROTRACE):
  errbar4 = plt.errorbar(N, LSOPRM_ctime, LSOPRM_ctime_err, None, 'r', ls='none', mew=3)
errbar5 = plt.errorbar(N, XPIR_ctime, XPIR_ctime_err, None, 'r', ls='none', mew=3)
errbar6 = plt.errorbar(N, SealPIR_ctime, SealPIR_ctime_err , None, 'r', ls='none', mew=3)

if(PLOT_PATHORAM):
  plt.setp(points1, 'mew', '3.0')
if(PLOT_CIRCUITORAM):
  plt.setp(points2, 'mew', '3.0')
if(PLOT_LSIPRM):
  plt.setp(points3, 'mew', '3.0')
if(PLOT_ZEROTRACE):
  plt.setp(points4, 'mew', '3.0')
plt.setp(points5, 'mew', '3.0')
plt.setp(points6, 'mew', '3.0')
#plt.setp(line1, 'linewidth', '2.0')
#plt.setp(line2, 'linewidth', '2.0')

ax = plt.subplot()
handles, labels = ax.get_legend_handles_labels()
labels_sort = [3,2,1]
hl = sorted(zip(handles, labels, labels_sort),
            key=operator.itemgetter(2))
handles2, labels2, labels_sort= zip(*hl)
#ax.legend(handles2, labels2, loc = 7)

plt.title('Client computation time (in ms) vs \nnumber of hidden service descriptors')
ax.legend(handles2, labels2, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=2, shadow=True)
plt.savefig('client_time_microbenchmark.png', bbox_inches='tight')
plt.close()

print("Done with client computation time graph..")
###################################################################

plt.xlabel('Number of Hidden Service Descriptors')
plt.ylabel('Total time per query (in ms)')
plt.grid(True)
plt.yscale('log')
plt.xscale('log')

c_ctr=0

if(PLOT_PATHORAM):
  points1 = plt.plot(N, PathORAM_time, POINT_STYLES[c_ctr], label='PathORAM',  color = str("C")+str(c_ctr))
if(PLOT_CIRCUITORAM):
  points2 = plt.plot(N, CircuitORAM_time, POINT_STYLES[c_ctr+1], label='CircuitORAM', color = str("C")+str(c_ctr+1))
if(PLOT_LSIPRM):
  points3 = plt.plot(N, LSIPRM_time, POINT_STYLES[c_ctr+2], label='Linear Scan (inside PRM)', color = str("C")+str(c_ctr+2))
if(PLOT_ZEROTRACE):
  points4 = plt.plot(N, LSOPRM_time, POINT_STYLES[c_ctr+3], label='Linear Scan (outside PRM)', color = str("C")+str(c_ctr+3))
points5 = plt.plot(N, XPIR_time, POINT_STYLES[c_ctr+4], label='XPIR', color = str("C")+str(c_ctr+4))
points6 = plt.plot(N, SealPIR_time, POINT_STYLES[c_ctr+5], label='SealPIR', color = str("C")+str(c_ctr+5))
#points7 = plt.plot(N, SealPIR_D1_time, POINT_STYLES[c_ctr+6], label='SealPIR D=1', color = str("C")+str(c_ctr+5))

if(PLOT_PATHORAM):
  errbar1 = plt.errorbar(N, PathORAM_time, PathORAM_time_err, None, 'r', ls='none', mew=3)
if(PLOT_CIRCUITORAM):
  errbar2 = plt.errorbar(N, CircuitORAM_time, CircuitORAM_time_err, None, 'r', ls='none', mew=3)
if(PLOT_LSIPRM):
  errbar3 = plt.errorbar(N, LSIPRM_time, LSIPRM_time_err, None, 'r', ls='none', mew=3)
if(PLOT_ZEROTRACE):
  errbar4 = plt.errorbar(N, LSOPRM_time, LSOPRM_time_err, None, 'r', ls='none', mew=3)
errbar5 = plt.errorbar(N, XPIR_time, XPIR_time_err, None, 'r', ls='none', mew=3)
errbar6 = plt.errorbar(N, SealPIR_time, SealPIR_time_err , None, 'r', ls='none', mew=3)

if(PLOT_PATHORAM):
  plt.setp(points1, 'mew', '3.0')
if(PLOT_CIRCUITORAM):
  plt.setp(points2, 'mew', '3.0')
if(PLOT_LSIPRM):
  plt.setp(points3, 'mew', '3.0')
if(PLOT_ZEROTRACE):
  plt.setp(points4, 'mew', '3.0')
plt.setp(points5, 'mew', '3.0')
plt.setp(points6, 'mew', '3.0')
#plt.setp(points7, 'mew', '3.0')
#plt.setp(line1, 'linewidth', '2.0')
#plt.setp(line2, 'linewidth', '2.0')

ax = plt.subplot()
handles, labels = ax.get_legend_handles_labels()
#labels_sort = [4,5,6,7,3,2,1]
labels_sort = [3,4,5,6,2,1]
hl = sorted(zip(handles, labels, labels_sort),
            key=operator.itemgetter(2))
handles2, labels2, labels_sort= zip(*hl)
#ax.legend(handles2, labels2, loc = 7)

plt.title('Total computation time (in ms) vs number of hidden service descriptors')
ax.legend(handles2, labels2, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=2, shadow=True)
plt.savefig('total_time_microbenchmark.png', bbox_inches='tight')
plt.close()


print("Done with total time graph..")
###################################################################

plt.xlabel('Number of Hidden Service Descriptors')
plt.ylabel('Request size per query (in bytes)')
plt.grid(True)
plt.yscale('log')
plt.xscale('log')

c_ctr=0

if(PLOT_ZEROTRACE):
  points4 = plt.plot(N, LSOPRM_query_size, POINT_STYLES[c_ctr+3], label='ZeroTrace', color = str("C")+str(c_ctr+3))
points5 = plt.plot(N, XPIR_REQUEST_SIZE, POINT_STYLES[c_ctr+4], label='XPIR', color = str("C")+str(c_ctr+4))
points6 = plt.plot(N, SealPIR_query_size, POINT_STYLES[c_ctr+5], label='SealPIR', color = str("C")+str(c_ctr+5))


if(PLOT_ZEROTRACE):
  plt.setp(points4, 'mew', '3.0')
plt.setp(points5, 'mew', '3.0')
plt.setp(points6, 'mew', '3.0')

ax = plt.subplot()
handles, labels = ax.get_legend_handles_labels()
labels_sort = [3,1,2]
hl = sorted(zip(handles, labels, labels_sort),
            key=operator.itemgetter(2))
handles2, labels2, labels_sort= zip(*hl)
#ax.legend(handles2, labels2, loc = 7)

plt.title('Request size (in bytes) vs \nnumber of hidden service descriptors')
ax.legend(handles2, labels2, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=2, shadow=True)
plt.savefig('request_size.png', bbox_inches='tight')
plt.close()

print("Done with request size graph..")
###################################################################

plt.xlabel('Number of Hidden Service Descriptors')
plt.ylabel('Response size per query (in bytes)')
plt.grid(True)
plt.yscale('log')
plt.xscale('log')

c_ctr=0

TrivialPIR_size = np.multiply(N, DESC_SIZE)

if(PLOT_ZEROTRACE):
  points4 = plt.plot(N, LSOPRM_response_size, POINT_STYLES[c_ctr+3], label='ZeroTrace', color = str("C")+str(c_ctr+3))
points5 = plt.plot(N, XPIR_RESPONSE_SIZE, POINT_STYLES[c_ctr+4], label='XPIR', color = str("C")+str(c_ctr+4))
points6 = plt.plot(N, SealPIR_response_size, POINT_STYLES[c_ctr+5], label='SealPIR', color = str("C")+str(c_ctr+5))
points7 = plt.plot(N, TrivialPIR_size, POINT_STYLES[c_ctr+6], label='Trivial PIR', color = str("C")+str(c_ctr+6))


if(PLOT_ZEROTRACE):
  plt.setp(points4, 'mew', '3.0')
plt.setp(points5, 'mew', '3.0')
plt.setp(points6, 'mew', '3.0')
plt.setp(points7, 'mew', '3.0')

ax = plt.subplot()
handles, labels = ax.get_legend_handles_labels()
labels_sort = [4,2,3,1]
hl = sorted(zip(handles, labels, labels_sort),
            key=operator.itemgetter(2))
handles2, labels2, labels_sort= zip(*hl)
#ax.legend(handles2, labels2, loc = 7)

plt.title('Response size (in bytes) vs \nnumber of hidden service descriptors')
ax.legend(handles2, labels2, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=4, shadow=True)
plt.savefig('response_size.png', bbox_inches='tight')
plt.close()

print("Done with response size graph..")
###################################################################

plt.xlabel('Number of Hidden Service Descriptors')
plt.ylabel('Total bandwidth per query (in bytes)')
plt.grid(True)
plt.yscale('log')
plt.xscale('log')

c_ctr=0

LSOPRM_bw = np.add(LSOPRM_query_size, LSOPRM_response_size)
XPIR_bw = np.add(XPIR_REQUEST_SIZE, XPIR_RESPONSE_SIZE)
SealPIR_bw = np.add(SealPIR_query_size, SealPIR_response_size)


if(PLOT_ZEROTRACE):
  points4 = plt.plot(N, LSOPRM_bw, POINT_STYLES[c_ctr+3], label='ZeroTrace', color = str("C")+str(c_ctr+3))
points5 = plt.plot(N, XPIR_bw, POINT_STYLES[c_ctr+4], label='XPIR', color = str("C")+str(c_ctr+4))
points6 = plt.plot(N, SealPIR_bw, POINT_STYLES[c_ctr+5], label='SealPIR', color = str("C")+str(c_ctr+5))
points7 = plt.plot(N, TrivialPIR_size, POINT_STYLES[c_ctr+6], label='Trivial PIR', color = str("C")+str(c_ctr+6))


if(PLOT_ZEROTRACE):
  plt.setp(points4, 'mew', '3.0')
plt.setp(points5, 'mew', '3.0')
plt.setp(points6, 'mew', '3.0')
plt.setp(points7, 'mew', '3.0')

ax = plt.subplot()
handles, labels = ax.get_legend_handles_labels()
#labels_sort = [4,1,3,2]
labels_sort = [4,3,2,1]
hl = sorted(zip(handles, labels, labels_sort),
            key=operator.itemgetter(2))
handles2, labels2, labels_sort= zip(*hl)
#ax.legend(handles2, labels2, loc = 7)

plt.title('Total bandwidth required (in bytes) vs \nnumber of hidden service descriptors')
ax.legend(handles2, labels2, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=4, shadow=True)
plt.savefig('total_bw.png', bbox_inches='tight')
plt.close()

print("Done with total bandwidth graph..")

###################################################################
