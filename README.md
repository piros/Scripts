Prerequisites:

## Install SGX driver

Install the SGX driver in the usual way.  This example is on a stock
Ubuntu 22.04.

```
apt install build-essential 
git clone https://github.com/intel/linux-sgx-driver
cd linux-sgx-driver
make
sudo make install
sudo /sbin/modprobe isgx
```

For plotting the graphs from the experiment data gathered, our plotter script requires python3, and the python modules numpy and matplotlib.


To reproduce our experiments:

1) Run ./build_dockers.sh to create the docker images to reproduce XPIR, SealPIR and ZeroTrace microbenchmarks. To build and run the ZeroTrace microbenchmarks, the host machine must have the SGX driver installed on it; refer to README.sgx.md for instructions on installing the SGX driver.
(This takes about 30 mins to complete)

2) Run ./reproduce_results.sh to run all the experiments in their respective dockers, populate the plotter folder with the output log files for these libraries and generate the graphs from these output logs.
(This takes about 15 mins to complete)

_______________________________________________________________________________

To run individual microbenchmarks on their own:

For SealPIR microbenchmark:

1) Go to the SPIR_docker folder

2) Build the SealPIR docker: docker build -t spir_docker .

3) Enter the docker with: docker run -it spir_docker

4) Set the desired configs for the experiments in config.sh

5) Run ./run_sealpir.sh to run the experiments and generate the output logfile (by default log_SEALPIR unless tweaked in config.sh)

6) Copy the output file out of the docker to the plotter folder docker cp -r <docker_id>:/pir/Results/XPIR ./plotter



For the XPIR microbenchmark:

1) Go to the XPIR_docker folder

2) Build the XPIR docker: docker build -t xpir_docker .

3) Enter the docker with: docker run -it xpir_docker

4) Set the desired configs for the experiments in config.sh

5) Run ./run_xpir.sh to run the experiments and generate the output folder (by default /pir/Results/ within the docker)

6) Copy the output folder out of the docker to the main scripts folder docker cp <docker_id>:/pir/log_Results ./plotter



For the ZeroTrace microbenchmark, ensure that the SGX driver is installed on the host machine. Refer to README.sgx.md for instructions to do so.

1) Go to the ZT_docker folder

2) Build the ZeroTrace docker: docker build -t zt_docker .

3) Start and enter the docker with : docker run --device /dev/isgx -it zt_docker bash -c "start-aesmd && exec bash" 

4) Set the desired configs in config.sh

5) run ./zt_lsoram.sh to generate the LinearScan ORAM benchmarks

6) run ./zt_hsoram.sh to generate the Circuit or Path ORAM benchmarks and use config.sh to set appropriate ORAM parameters for them.

7) Copy the results to outside of the docker for replotting graphs with our grapher script: docker cp <docker_id>:/pir/log_CIRCUITORAM ./plotter
