#!/bin/bash

SGX_DRIVER_FILE=/dev/isgx
test -e "$SGX_DRIVER_FILE" && SGX_FLAG=1 || SGX_FLAG=0
if [ $SGX_FLAG == "1" ]
then
  docker run --device /dev/isgx -dt --name zt_container zt_image bash -c "start-aesmd && exec bash"
  docker exec -t zt_container bash -c "cd /pir/ && ./run_ztlsoram.sh"
  docker exec -t zt_container bash -c "cd /pir/ && ./run_zthsoram.sh"
  docker cp zt_container:/pir/log_ZTLSORAM ./plotter
  docker cp zt_container:/pir/log_ZTCIRCUITORAM ./plotter
  docker rm -f zt_container
else
  echo "Skipping ZeroTrace since the device does not have SGX driver installed."
fi

docker run -dt --name xpir_container xpir_image
docker exec -t xpir_container bash -c "cd /pir/ && ./run_xpir.sh"
docker cp xpir_container:/pir/Results/XPIR ./plotter
docker rm -f xpir_container

docker run -dt --name spir_container spir_image
docker exec -t spir_container bash -c "cd /pir/ && ./run_sealpir.sh"
docker cp spir_container:/pir/log_SEALPIR ./plotter
docker rm -f spir_container

cd ./plotter && ./gen_graphs.py $SGX_FLAG
